/**
 * @file
 * Defines Javascript behaviors for the cookies module.
 */;

(function (Drupal, $) {
  'use strict';

  /**
   * Define defaults.
   */
  Drupal.behaviors.cern_cookies_management = {

    cds_activate: function (context) {
      // Called when Tracking cookies are enabled
      jQuery('iframe[data-sid="cern_embedded_cds_videos"]').each(function() {
        jQuery(this).attr('src', jQuery(this).data('src'));
      });
    },

    ga_activate: function (context) {
      // Called when Tracking cookies are enabled
      jQuery('iframe[data-sid="cern_embedded_ga_videos"]').each(function() {
        jQuery(this).attr('src', jQuery(this).data('src'));
      });
    },

    cds_fallback: function (context) {
      // Called when Tracking cookies are disabled
      // for each iframe, checks if the video comes from CDS or Youtube or Vimeo and adds the cookie overlay
      $("iframe").each(function () {
        let src = $(this).attr('src')
        if (src.includes("cds.cern.ch/video")){
          // replace src with empty string so that the iframe (and hence its scripts) will not be loaded
          $(this).attr('src', '');
          // store src in data-src in order to be easily retrieved when the user activates the cookies
          $(this).attr('data-src', src);
          $(this).attr('data-sid', 'cern_embedded_cds_videos');
          $(this, context).cookiesOverlay('cern_embedded_cds_videos');
        }
      })
    },

    ga_fallback: function (context) {
      // Called when Tracking cookies are disabled
      // for each iframe, checks if the video comes from Youtube or Vimeo and adds the cookie overlay
      $("iframe").each(function () {
        let src = $(this).attr('src')
        if (src.includes("youtube.com") || src.includes("vimeo.com")) {
          // replace src with empty string so that the iframe (and hence its scripts) will not be loaded
          $(this).attr('src', '');
          // store src in data-src in order to be easily retrieved when the user activates the cookies
          $(this).attr('data-src', src);
          $(this).attr('data-sid', 'cern_embedded_ga_videos');
          $(this, context).cookiesOverlay('cern_embedded_ga_videos');
        }
      })
    },

    attach: function (context) {
      let self = this;
      document.addEventListener('cookiesjsrUserConsent', function (event) {
        let service = (typeof event.detail.services === 'object') ? event.detail.services : {};
        if (typeof service['cern_embedded_cds_videos'] !== 'undefined' && service['cern_embedded_cds_videos']) {
          self.cds_activate(context);
        } else {
          self.cds_fallback(context);
        }

        if (typeof service['cern_embedded_ga_videos'] !== 'undefined' && service['cern_embedded_ga_videos']) {
          self.ga_activate(context);
        } else {
          self.ga_fallback(context);
        }

        self.replace_text(context);
      });
    },

    /**
     * The text is replaced using JS because it is pre-defined in the templates of the core module
     * @param context
     */
    replace_text: function (context) {
      let embedded_video_cookies_text = $("div.cookies-fallback--text");
      if (embedded_video_cookies_text.text().indexOf("Cern_embedded_cds_videos") >= 0){
        embedded_video_cookies_text.text(embedded_video_cookies_text.text().replace(/Cern_embedded_cds_videos/g,'Matomo Analytics'));
        let embedded_video_cookies_button = $(".cookies-fallback--cern_embedded_cds_videos .cookies-fallback--link");
        embedded_video_cookies_button.text(embedded_video_cookies_button.text().replace(/Cern_embedded_cds_videos/g,'Matomo Analytics'));
      }
      else if (embedded_video_cookies_text.text().indexOf("Cern_embedded_ga_videos") >= 0){
        embedded_video_cookies_text.text(embedded_video_cookies_text.text().replace(/Cern_embedded_ga_videos/g,'Google Analytics'));
        let embedded_video_cookies_button = $(".cookies-fallback--cern_embedded_ga_videos .cookies-fallback--link");
        embedded_video_cookies_button.text(embedded_video_cookies_button.text().replace(/Cern_embedded_ga_videos/g,'Google Analytics'));
      }
    }
  }
})(Drupal, jQuery);
